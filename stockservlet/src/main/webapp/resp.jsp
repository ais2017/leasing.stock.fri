<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>Responsible</title>

<script>
            
            function changedText(val)
            {
                switch(val){
                    case ("sel1"):
                         document.getElementById('inputSel1').style.visibility="hidden";
                         document.f.constraint.value = "all";
                        //document.f.submit();
                         break;
                         
                    case ("sel2"):
                         document.getElementById('inputSel1').style.visibility="visible";
                         document.f.constraint.value = "name";
                         break;
                         
                    case ("sel3"):
                         document.getElementById('inputSel1').style.visibility="visible";
                         document.f.constraint.value = "place";
                         break;
                }
                
            }
        </script>
</head>
<body>
<table width="100%" cellpadding="0" cellspacing="0">
            <tr>
            <p><a href="resp_add.jsp">Manage equipment</a></p>
            </tr>
            <tr>
                <td width="30%">
                    <form name="f" method="post" action="<c:url value='/main'/>">
                        <input type="hidden" name="op" value="show">
                         <h4>Show stock contents</h4>
                         
                        <table width="25%" cellspacing="0" cellpadding="4">
                            <tr>
                                <td align="right" width="75">                            
                                    <select name="filterType" onchange="changedText(this.value)">
                                        <option value="sel1">All</option>
                                        <option value="sel2">Name</option>
                                        <option value="sel3">Place</option>
                                    </select>  
                                </td>
                                <td align="left"><input type="text" id=inputSel1 name="filterParam" value="" size="15" /></td>
                            </tr>
                            <tr>
                                <td align ="right"><input type="button" value="Show" onclick="f.submit();"></td>
                                <td><input type="hidden" name="constraint" value="zero"></td>
                        </table>
                
                    </form>
                </td>
                <td valign="top">
                  <form method="post" action="<c:url value='/main'/>">
                    <table border="1" align="center" cellpadding="3" cellspacing="3" width="100%" style="border-collapse: collapse; border: 1px solid black;">
                        <tr><th>Name</th>
                        <th>Place</th>
                        <th>Complexity</th>
                        </tr>
                        <c:forEach var="equipment" items="${equipments}">
                            <tr>
                                <td>${equipment.name}</td>
                                <td>${equipment.place}</td>
                                <td>${equipment.compl}</td>
                            </tr>
                        </c:forEach>
                    </table>
                    </form>
                </td>
            </tr>
        </table>
</body>
</html>