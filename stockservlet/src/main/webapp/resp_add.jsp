<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>New equipment</title>
<script type="text/javascript">
function checkEdit(){
    
    if(document.f.name.value === "")
        return false;
    
    if(document.f.date.value === "")
        return false;
    
    if(document.f.exp.value === "")
        return false;
    
    if(document.f.source.value === "")
        return false;
    
    if(document.f.compl.value === "")
        return false;
    
    return true;
}

function checkFormAdd()
{
          if(!checkEdit()){
              alert("Empty fields!");
          return false;
          }
          else{
        	  document.f.date = document.f.dateField;
              document.f.submit();
          }
}

function checkFormRm()
{
          if(document.rm_eq.name.value === ""){
              alert("Empty fields!");
          return false ;
          }
          else{
              document.rm_eq.submit();
          }
}

</script>
</head>
<body>

<table width="100%" cellpadding="0" cellspacing="0">
<tr>
                <td width="30%">
                    <form name="f" method="post" action="<c:url value='/main'/>">
                    <input type="hidden" name="op" value="add">
                    <table width="25%" cellspacing="0" cellpadding="4">
                    <tr><th>Add</th></tr>
                            <tr> 
                                <td align="right" width="75">Name</td>
                                <td><input type="text" name="name" maxlength="50" size="15"></td>
                            </tr>
                            <tr> 
                                <td align="right">Date</td>
                                <td><input type="date" name="dateField" maxlength="50" size="15"></td>
                            </tr>
                            <tr> 
                                <td align="right">Expiring</td>
                                <td><input type="date" name="expField" maxlength="50" size="15"></td>
                            </tr>
                            <tr> 
                                <td align="right">Source</td>
                                <td><input type="text" name="source" maxlength="50" size="15"></td>
                            </tr>
                            <tr> 
                                <td align="right">Complexity</td>
                                <td><select name="compl">
                                  <option value="1">1</option>
                                  <option value="2">2</option>
                                  <option value="3">3</option>
                                  </select>
                                  </td>
                            </tr>
                            <tr>
                            <td>
                            </td>
                            <td align="right">Acceptance</td>
                            </tr>
                            <tr>
                            <td align="right">Technical employee Id</td>
                            <td><input type="text" name="techId" maxlength="50" size="15"></td>
                            </tr>
                            <tr>
                            <td align="right">Acceptance employee Id</td>
                            <td><input type="text" name="accId" maxlength="50" size="15"></td>
                            </tr>
                            <tr>
                            <td align="right">Customer</td>
                            <td><input type="text" name="customer" maxlength="50" size="15"></td>
                            </tr>
                            <tr> 
                                <td></td>
                                <td><input type="submit" value="Add equipment" onclick="checkFormAdd();"></td>
                            </tr>
                            <tr>
                            </tr>
                        </table>
                        </form>
                        </td>
                        </tr>
                        <tr>
                        <td>
                        <form name="rm_eq" method="post" action="<c:url value='/main'/>">
                        <input type="hidden" name="op" value="rm">
                        <table width="25%" cellspacing="0" cellpadding="4">
                        <tr><th>Remove</th></tr>
                        <tr>
                        <td align="right">Equipment name</td>
                        <td><input type="text" name="name" maxlength="50" size="15"></td>
                        <td><input type="submit" value="Rm equipment" onclick="checkFormRm();"></td>
                        </tr>
                        </table>
                        </form>
                        </td>
                        </tr>
                        </table>
</body>
</html>