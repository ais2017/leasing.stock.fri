<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<title>Technical</title>
<script>
            
            function changedText(val)
            {
                switch(val){
                    case ("sel1"):
                         document.getElementById('inputSel1').style.visibility="hidden";
                         document.f.constraint.value = "all";
                         break;
                         
                    case ("sel2"):
                         document.getElementById('inputSel1').style.visibility="visible";
                         document.f.constraint.value = "name";
                         break;
                         
                    case ("sel3"):
                         document.getElementById('inputSel1').style.visibility="visible";
                         document.f.constraint.value = "place";
                         break;
                }
                
            }
            
            function doTransfer(){
            	if(document.addch.id.value === "" || 
            			document.addch.result.value === "" ||
            			document.addch.date.value === "" ||
            			document.addch.eq_name.value === ""){
            		alert("Empty fields!");
            	    return false;
            	}
            	else{
            		if(document.getElementById('successFlag').checked){
            			document.addch.success.value = "ok";
            		}
            		document.addch.submit();
            		
            	}
            }
            
        </script>
</head>
<body>
<table width="100%" cellpadding="0" cellspacing="0">
            <tr>
                <td width="30%">
                    <form name="f" method="post" action="<c:url value='/tech'/>">
                        <input type="hidden" name="op" value="show">
                         <h4>Show stock contents</h4>
                         
                        <table width="25%" cellspacing="0" cellpadding="4">
                            <tr>
                                <td align="right" width="75">                            
                                    <select name="filterType" onchange="changedText(this.value)">
                                        <option value="sel1">All</option>
                                        <option value="sel2">Name</option>
                                        <option value="sel3">Place</option>
                                    </select>  
                                </td>
                                <td align="left"><input type="text" id=inputSel1 name="filterParam" value="" size="15" /></td>
                            </tr>
                            <tr>
                                <td align ="right"><input type="button" value="Show" onclick="f.submit();"></td>
                                <td><input type="hidden" name="constraint" value="zero"></td>
                        </table>
                
                    </form>
                </td>
                <td valign="top">
                  <form method="post" action="<c:url value='/tech'/>">
                    <table border="1" align="center" cellpadding="3" cellspacing="3" width="100%" style="border-collapse: collapse; border: 1px solid black;">
                        <tr><th>Name</th>
                        <th>Place</th>
                        <th>Complexity</th>
                        </tr>
                        <c:forEach var="equipment" items="${equipments}">
                            <tr>
                                <td>${equipment.name}</td>
                                <td>${equipment.place}</td>
                                <td>${equipment.compl}</td>
                            </tr>
                        </c:forEach>
                    </table>
                    </form>
                </td>
            </tr>
            <tr><th>Add check</th></tr>
            <tr>
            <td>
            <form name="addch" method="post" action="<c:url value='/tech'/>">
            <input type="hidden" name="op" value="add_chk">
            <input type="hidden" name="success" value="">
            <table width="25%" cellspacing="0" cellpadding="4">
            <tr>
              <td align="right" width="75">Employee Id:</td>
              <td> <input type="text" name="id" size="15"></td>
            </tr>
            <tr>
              <td align="right">Equipment name: </td>
              <td><input type="text" name="eq_name" size="15"></td>
            </tr>
            <tr>
              <td align="right">Result: </td>
              <td><input type="text" name="result" size="15"></td>
            </tr>
            <tr>
              <td align="right">Success: </td>
              <td><input type="checkbox" id="successFlag" size="15"></td>
            </tr>
            <tr>
              <td align="right">Date: </td>
              <td><input type="date" name="date" size="15"></td>
            </tr>
            <tr>
            <td></td>
            <td><input type="submit" value="Add" onclick="doTransfer();"></td>
            </tr>
            </table>
            </form>
            </td>
            </tr>
        </table>
</body>
</html>