package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class Place {

    private final String section;
    private Equipment currentEquipment;

    public boolean isEmpty() {
        return currentEquipment == null;
    }

    public boolean setEquipment(Equipment equipment) {
        if(currentEquipment != null) {
            throw new RuntimeException("Not empty place");
        }
        this.currentEquipment = equipment;
        return true;
    }

    public Equipment getAndClear() {
        if (currentEquipment == null) throw new RuntimeException("Empty place");
        Equipment equipment = currentEquipment;
        currentEquipment = null;
        return equipment;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Place place = (Place) o;

        return section != null ? section.equals(place.section) : place.section == null;

    }

    @Override
    public int hashCode() {
        return section != null ? section.hashCode() : 0;
    }
}
