package com.example.demo.dto;

import com.example.demo.dao.entity.PlaceEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class EquipmentDto {

    private Integer id;
    private Integer technicalEmployee;
    private Integer acceptEmployee;
    private String customer;
    private  String name;
    private Date receiptDate;
    private  Date expiryDate;
    private  String source;
    private  int complexity;
    private PlaceEntity place;

}
