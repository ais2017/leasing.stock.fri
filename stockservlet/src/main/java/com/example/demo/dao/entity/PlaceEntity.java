package com.example.demo.dao.entity;

import groovy.transform.ToString;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@ToString
@Getter
@Setter
public class PlaceEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private String section;
    @OneToOne(fetch = FetchType.LAZY , cascade = CascadeType.ALL)
    private EquipmentEntity currentEquipment;

    public PlaceEntity() {
    }

    public PlaceEntity(Integer id, String section, EquipmentEntity currentEquipment) {
    	this.id = id;
        this.section = section;
        this.currentEquipment = currentEquipment;
    }
    
    public PlaceEntity(String section, EquipmentEntity currentEquipment) {
    	this.id = section.hashCode();
        this.section = section;
        this.currentEquipment = currentEquipment;
    }

	@Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PlaceEntity entity = (PlaceEntity) o;

        if (id != null ? !id.equals(entity.id) : entity.id != null) return false;
        return section != null ? section.equals(entity.section) : entity.section == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (section != null ? section.hashCode() : 0);
        return result;
    }
}
