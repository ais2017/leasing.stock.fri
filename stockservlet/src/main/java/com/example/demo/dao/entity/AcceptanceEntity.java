package com.example.demo.dao.entity;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@ToString
@AllArgsConstructor
@Getter
@Setter
public class AcceptanceEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @OneToOne(fetch = FetchType.LAZY)
    private CustomerEntity customer;
    @OneToOne(fetch = FetchType.LAZY)
    private EmployeeEntity technicalEmployee;
    @OneToOne(fetch = FetchType.LAZY)
    private EmployeeEntity accepterEmployee;
    private Date dateOfAccept;
    private String type;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        AcceptanceEntity that = (AcceptanceEntity) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (technicalEmployee != null ? !technicalEmployee.equals(that.technicalEmployee) : that.technicalEmployee != null)
            return false;
        if (accepterEmployee != null ? !accepterEmployee.equals(that.accepterEmployee) : that.accepterEmployee != null)
            return false;
        if (dateOfAccept != null ? !dateOfAccept.equals(that.dateOfAccept) : that.dateOfAccept != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (customer != null ? customer.hashCode() : 0);
        result = 31 * result + (technicalEmployee != null ? technicalEmployee.hashCode() : 0);
        result = 31 * result + (accepterEmployee != null ? accepterEmployee.hashCode() : 0);
        result = 31 * result + (dateOfAccept != null ? dateOfAccept.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
