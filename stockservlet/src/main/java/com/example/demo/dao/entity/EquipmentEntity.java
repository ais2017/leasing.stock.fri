package com.example.demo.dao.entity;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Entity
@ToString
@Getter
@Setter
//@AllArgsConstructor
public class EquipmentEntity {

    @Id
    @GeneratedValue
    private Integer id;
    private  String name;
    private  Date receiptDate;
    private  Date expiryDate;
    private  String source;
    @OneToMany(fetch = FetchType.LAZY)
    private  List<AcceptanceEntity> acceptanceList;
    @OneToMany(fetch = FetchType.LAZY)
    private  List<CheckEntity> technicalChecks;
    private  int complexity;
    @OneToOne(fetch = FetchType.LAZY)
    private PlaceEntity place;

    public EquipmentEntity(Integer id, String name, Date receiptDate, Date expiryDate, String source, int complexity, PlaceEntity entity) {
        //this.id = id;
        this.name = name;
        this.receiptDate = receiptDate;
        this.expiryDate = expiryDate;
        this.source = source;
        this.complexity = complexity;
        this.id = (id != null) ? id : this.hashCode();
        acceptanceList = new ArrayList<>();
        technicalChecks = new ArrayList<>();
        this.place = entity;
    }
    
    public EquipmentEntity(Integer id, String name, Date receiptDate, Date expiryDate, String source ,
    		 List<AcceptanceEntity> acceptanceList, List<CheckEntity> technicalChecks, int complexity, PlaceEntity place) {
        //this.id = id;
        this.name = name;
        this.receiptDate = receiptDate;
        this.expiryDate = expiryDate;
        this.source = source;
        this.complexity = complexity;
        this.id = (id != null) ? id : this.hashCode();
        this.acceptanceList = acceptanceList;
        this.technicalChecks = technicalChecks;
        this.place = place;
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {


        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EquipmentEntity that = (EquipmentEntity) o;

        if (complexity != that.complexity) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        if (receiptDate != null ? !receiptDate.equals(that.receiptDate) : that.receiptDate != null) return false;
        if (expiryDate != null ? !expiryDate.equals(that.expiryDate) : that.expiryDate != null) return false;
        if (source != null ? !source.equals(that.source) : that.source != null) return false;
        if (acceptanceList != null ? !acceptanceList.equals(that.acceptanceList) : that.acceptanceList != null)
            return false;
        return technicalChecks != null ? technicalChecks.equals(that.technicalChecks) : that.technicalChecks == null;

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (receiptDate != null ? receiptDate.hashCode() : 0);
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (acceptanceList != null ? acceptanceList.hashCode() : 0);
        result = 31 * result + (technicalChecks != null ? technicalChecks.hashCode() : 0);
        result = 31 * result + complexity;
        return result;
    }
}
