package com.example.demo.service.transform;

import com.example.demo.dao.entity.EquipmentEntity;
import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;

public abstract class PlaceTransform {

    public static PlaceEntity transformToEntity(Place place) {
        PlaceEntity entity = new PlaceEntity(place.getSection(), null);
        Equipment equip = place.getCurrentEquipment();
        if (equip != null) {
            EquipmentEntity currentEquipment = EquipmentTransform.transformToEntity(equip);
            entity.setCurrentEquipment(currentEquipment);
            currentEquipment.setPlace(entity);
        }
        return entity;
    }

    public static Place transformFromEntity(PlaceEntity entity) {
        Place place = new Place(entity.getSection(), null);
        EquipmentEntity equip = entity.getCurrentEquipment();
        if (equip != null) {
            Equipment equipment = EquipmentTransform.transformFromEntity(equip);
            place.setEquipment(equipment);
        }
        return place;
    }

}
