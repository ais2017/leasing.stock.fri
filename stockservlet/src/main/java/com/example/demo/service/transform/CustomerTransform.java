package com.example.demo.service.transform;

import com.example.demo.dao.entity.CustomerEntity;
import com.example.demo.domain.Customer;

public abstract class CustomerTransform {

    public static CustomerEntity transformToEntity(Customer customer) {
        return new CustomerEntity(null, customer.getName(), customer.getSurname(), customer.getPosition());
    }

    public static Customer transformFromEntity(CustomerEntity customerEntity) {
        return new Customer(customerEntity.getName(), customerEntity.getSurname(), customerEntity.getPosition());
    }

}
