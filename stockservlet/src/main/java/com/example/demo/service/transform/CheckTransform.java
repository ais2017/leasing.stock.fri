package com.example.demo.service.transform;

import com.example.demo.dao.entity.CheckEntity;
import com.example.demo.domain.Check;
import com.example.demo.domain.Employee;
import com.example.demo.dto.CheckDto;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class CheckTransform {

    @Autowired

    public static CheckEntity transformToEntity(Check check) {
        return new CheckEntity(
                check.hashCode(),
                EmployeeTransform.transformToEntity(check.getTechnicalEmployee()),
                check.getResult(),
                check.isSuccess(),
                check.getDate());
    }

    public static Check transformFromDto(CheckDto check) {
        return new Check(
        		new Employee(check.getEmployeeId().toString(), null, null, null, null),
                check.getResult(),
                check.isSuccess(),
                check.getDate());
    }

    public static Check transformFromEntity(CheckEntity checkEntity) {
        return new Check(
                EmployeeTransform.transformFromEntity(checkEntity.getTechnicalEmployee()),
                checkEntity.getResult(),
                checkEntity.isSuccess(),
                checkEntity.getDate()
        );
    }

}
