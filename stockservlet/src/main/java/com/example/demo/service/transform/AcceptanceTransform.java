package com.example.demo.service.transform;

import com.example.demo.dao.entity.AcceptanceEntity;
import com.example.demo.domain.Acceptance;

public abstract class AcceptanceTransform {


    public static AcceptanceEntity transformToEntity(Acceptance acceptance) {
        return new AcceptanceEntity(null,
                CustomerTransform.transformToEntity(acceptance.getCustomer()),
                EmployeeTransform.transformToEntity(acceptance.getTechnicalEmployee()),
                EmployeeTransform.transformToEntity(acceptance.getAccepterEmployee()),
                acceptance.getDateOfAccept(),
                acceptance.getType());
    }

    public static Acceptance transformFromEntity(AcceptanceEntity entity) {
        return new Acceptance(
                CustomerTransform.transformFromEntity(entity.getCustomer()),
                EmployeeTransform.transformFromEntity(entity.getTechnicalEmployee()),
                EmployeeTransform.transformFromEntity(entity.getAccepterEmployee()),
                entity.getDateOfAccept(),
                entity.getType()
            );
    }


}
