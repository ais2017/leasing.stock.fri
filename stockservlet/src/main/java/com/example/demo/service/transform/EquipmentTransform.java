package com.example.demo.service.transform;

import com.example.demo.dao.entity.EquipmentEntity;
import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Acceptance;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Employee;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dto.EquipmentDto;

import java.util.stream.Collectors;

public abstract class EquipmentTransform {


    public static EquipmentEntity transformToEntity(Equipment equipment) {
        return new EquipmentEntity(
                equipment.hashCode(),
                equipment.getName(),
                equipment.getReceiptDate(),
                equipment.getExpiryDate(),
                equipment.getSource(),
                equipment.getAcceptanceList().stream().map(AcceptanceTransform::transformToEntity).collect(Collectors.toList()),
                equipment.getTechnicalChecks().stream().map(CheckTransform::transformToEntity).collect(Collectors.toList()),
                equipment.getComplexity(),
                equipment.getPlace() == null ? null : new PlaceEntity(1, equipment.getPlace().getSection(), null)
                );
    }

    public static Equipment transformFromEntity(EquipmentEntity equipmentEntity) {
        return new Equipment(
                equipmentEntity.getName(),
                equipmentEntity.getReceiptDate(),
                equipmentEntity.getExpiryDate(),
                equipmentEntity.getSource(),
                equipmentEntity.getAcceptanceList().stream().map(AcceptanceTransform::transformFromEntity).collect(Collectors.toList()),
                equipmentEntity.getTechnicalChecks().stream().map(CheckTransform::transformFromEntity).collect(Collectors.toList()),
                equipmentEntity.getComplexity(),
                equipmentEntity.getPlace() == null ? null : new Place(equipmentEntity.getPlace().getSection(), null)
                );
    }



    public static Equipment transformFromDto(EquipmentDto equipment) {
        Equipment eq = new Equipment(
                equipment.getName(),
                equipment.getReceiptDate(),
                equipment.getExpiryDate(),
                equipment.getSource(),
                equipment.getComplexity(),
                equipment.getPlace() == null ? null : new Place(equipment.getPlace().getSection(), null)
        );
        
        Acceptance accpt = new Acceptance(
        		new Customer(equipment.getCustomer(), equipment.getCustomer(), equipment.getCustomer()),
        		new Employee(equipment.getTechnicalEmployee().toString(), equipment.getTechnicalEmployee().toString(),
        				equipment.getTechnicalEmployee().toString(), equipment.getTechnicalEmployee().toString(),
        				equipment.getTechnicalEmployee().toString()),
        		new Employee(equipment.getAcceptEmployee().toString(), equipment.getAcceptEmployee().toString(), 
        				equipment.getAcceptEmployee().toString(), equipment.getAcceptEmployee().toString(),
        				equipment.getAcceptEmployee().toString()),
        		equipment.getReceiptDate(),
        		equipment.getSource()
        		);
        
        eq.addAcceptance(accpt);
        return eq;
    }
}
