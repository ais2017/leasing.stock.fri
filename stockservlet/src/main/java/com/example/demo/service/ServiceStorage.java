package com.example.demo.service;

import com.example.demo.dao.entity.EmployeeEntity;
import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dto.CheckDto;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.service.transform.CheckTransform;
import com.example.demo.service.transform.EquipmentTransform;
import com.example.demo.service.transform.PlaceTransform;
import com.example.demo.exchange.*;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ServiceStorage {
	
	private DataBaseGateway dbGateway;
	
    public List<Equipment> checkStorage() {
    	return dbGateway.getAllEquipment()
        		.stream()
        		.map(e -> EquipmentTransform.transformFromEntity(e))
                .collect(Collectors.toList());
    }

    public boolean transferEquipment(String fromPlaceId, String toPlaceId) {
    	
    	PlaceEntity fromEntity = dbGateway.getPlace(fromPlaceId.hashCode());
    	PlaceEntity toEntity = dbGateway.getPlace(toPlaceId.hashCode());
        
    	if(fromEntity == null || toEntity == null)
    		throw new RuntimeException("No such place");
    	
    	Place fromPlace = PlaceTransform.transformFromEntity(dbGateway.getPlace(fromPlaceId.hashCode()));
        Place toPlace = PlaceTransform.transformFromEntity(dbGateway.getPlace(toPlaceId.hashCode()));

        if (!fromPlace.isEmpty() && toPlace.isEmpty()) {
        	
            toPlace.setEquipment(fromPlace.getAndClear());
             dbGateway.updatePlace(fromPlaceId.hashCode(), PlaceTransform.transformToEntity(fromPlace));
             dbGateway.updatePlace(toPlaceId.hashCode(), PlaceTransform.transformToEntity(toPlace));
             
            return true;
        }
        
        throw new RuntimeException("Place error");

    }

    
    public List<Equipment> getEquipmentForCheck(Predicate<Equipment> filter) {
        List<Equipment> equipments = dbGateway.getAllEquipment()
        		.stream()
        		.map(e -> EquipmentTransform.transformFromEntity(e))
                .collect(Collectors.toList());
        
        return equipments.stream().filter(filter).collect(Collectors.toList());
    }

    public boolean addCheckOfEquipment(Equipment equipmentDto, CheckDto check) {
    	
    	if(!dbGateway.employeeExists(check.getEmployeeId()))
    		throw new RuntimeException("Employee not found");
        PlaceEntity entity = dbGateway.getPlaceWithEquipment(equipmentDto.getName());
        if(entity != null ) {
        	Place place = PlaceTransform.transformFromEntity(entity);
        	place.getCurrentEquipment().addCheck(CheckTransform.transformFromDto(check));
            entity = PlaceTransform.transformToEntity(place);
            dbGateway.updatePlace(place.getSection().hashCode(), entity);
            return true;
        }
        throw new RuntimeException("No such equipment");
    }

    public Place addNewEquipment(EquipmentDto equipmentDto) {
    	
    	if(!dbGateway.employeeExists(equipmentDto.getTechnicalEmployee()) ||
    			!dbGateway.employeeExists(equipmentDto.getAcceptEmployee()))
    		throw new RuntimeException("Employee not found");
    	
        Equipment equipment = EquipmentTransform.transformFromDto(equipmentDto);
        List<Place> collect = dbGateway.getEmptyPlaces()
        		.stream()
        		.map(e -> PlaceTransform.transformFromEntity(e))
                .collect(Collectors.toList());
        
        if(!collect.isEmpty()){
        	Place value = collect.get(0);
            value.setEquipment(equipment);
            dbGateway.updatePlace(value.getSection().hashCode(), PlaceTransform.transformToEntity(value));
            return value;
        }
        throw new RuntimeException("No free places");
    }

    public Equipment getEquipmentFromPlace(String placeId) {
    	Place place = PlaceTransform.transformFromEntity(dbGateway.getPlace(placeId.hashCode()));
        if (!place.isEmpty()) {
            return place.getCurrentEquipment();
        } else {
            throw new RuntimeException("Empty place");
        }
    }
    
    public Equipment getEquipmentFromStorage(String equipmentName) {
    	PlaceEntity entity = dbGateway.getPlaceWithEquipment(equipmentName);
    	
        if (entity != null) {
            return PlaceTransform.transformFromEntity(entity).getCurrentEquipment();
        } else {
            throw new RuntimeException("No such equipment");
        }
    }

    public Equipment getEquipmentFromStorageAndClear(String placeId) {
    	PlaceEntity entity = dbGateway.getPlace(placeId.hashCode());
    	
    	if(entity == null)
    		throw new RuntimeException("No such place");
    	
    	Place place = PlaceTransform.transformFromEntity(entity);
        if (!place.isEmpty()) {
            Equipment equip = place.getAndClear();
            dbGateway.updatePlace(placeId.hashCode(), PlaceTransform.transformToEntity(place));
            return equip;
        } else {
            throw new RuntimeException("Empty place");
        }
    }


}
