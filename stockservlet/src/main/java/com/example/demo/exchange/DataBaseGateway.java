package com.example.demo.exchange;

import java.util.*;
import org.sqlite.JDBC;

import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dao.entity.*;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;

// Handling all database operations, 
//  such as connecting, querying, date parsing.
// Class need to be changed if new database is applied (now using embedded sqlite)
public class DataBaseGateway {
	
	private Connection conn = null;
	
	SimpleDateFormat format = new SimpleDateFormat("MM-dd-yyyy");
	
	private static DataBaseGateway instance = null;
	
	public static DataBaseGateway getInstance() throws SQLException {
		if(instance == null)
			instance = new DataBaseGateway();
		return instance;
	}
	
	private DataBaseGateway() throws SQLException {
		String url = "jdbc:sqlite:database/databasetest.db";
		DriverManager.registerDriver(new JDBC());
		conn = DriverManager.getConnection(url);
	}
	
	public void createInitial() {
		
		try(Statement st = conn.createStatement()){
			// rm if exists
			st.execute("drop table if exists employee;");
			st.execute("drop table if exists acceptance;");
			st.execute("drop table if exists checks;");
			st.execute("drop table if exists placemap;");
			
			// create sample employee table
			st.execute("create table employee("
					+ "emp_id integer primary key,"
					+ "emp_name text,"
					+ "emp_position text"
					+ ");");
			
			st.execute("insert into employee "
					+ "values(1, \"darth vader\", \"employee\");");
			st.execute("insert into employee "
					+ "values(2, \"c3pio\", \"employee\");");
			st.execute("insert into employee "
					+ "values(3, \"r2d2\", \"employee\");");
			st.execute("insert into employee "
					+ "values(4, \"john shooter\", \"farmer\");");
			st.execute("insert into employee "
					+ "values(5, \"luke\", \"employee\");");
			st.execute("insert into employee "
					+ "values(6, \"yoda\", \"employee\");");
			st.execute("insert into employee "
					+ "values(7, \"chewbacca\", \"employee\");");
			
			// create empty acceptance table
			st.execute("create table acceptance("
					+ "eq_name text primary key,"
					+ "customer_id text,"
					+ "techEmpl_id integer,"
					+ "acEmpl_id integer,"
					+ "ac_date text,"
					+ "ac_type text,"
					+ "foreign key(techEmpl_id) references employee(emp_id),"
					+ "foreign key(acEmpl_id) references employee(emp_id),"
					+ "foreign key(eq_name) references placemap(eq_name));");
			
			// create empty check table
			st.execute("create table checks("
					+ "ch_id integer primary key,"
					+ "eq_name text,"
					+ "techEmpl_id integer,"
					+ "result text,"
					+ "isSuccess integer,"
					+ "date text,"
					+ "foreign key(techEmpl_id) references employee(emp_id),"
					+ "foreign key(eq_name) references placemap(eq_name));");
			
			// create placemap table with empty places
			st.execute("create table placemap("
					+ "place_id integer alternative key,"
					+ "section text primary key,"
					+ "equipment_id integer,"
					+ "eq_name text alternative key,"
					+ "eq_rcptDate text,"
					+ "eq_exprDate text,"
					+ "eq_source text,"
					+ "eq_complexity integer);");
			
			createPlace("1");
			createPlace("2");
			createPlace("3");
			createPlace("4");
			createPlace("5");
			createPlace("6");
			createPlace("7");
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void createPlace(String section) {
		PlaceEntity newPlace = new PlaceEntity(section.hashCode(), section, null);
		
		try(Statement st = conn.createStatement()){
			st.execute("insert into placemap(place_id, section) "
					+ "values(" + section.hashCode() + ", \"" + section + "\");");
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
	}
	
	public boolean employeeExists(int id) {
		
		int number = 0;
		try(Statement st = conn.createStatement()){
			ResultSet rs = st.executeQuery("select count(*) as total from employee where emp_id = " + id + ";");
			number = rs.getInt("total");
		}
		catch(SQLException e) {
			e.printStackTrace();
		}
		
		return number == 1;
	}
	
	public List<EquipmentEntity> getAllEquipment(){
		
		try (Statement statement = conn.createStatement()) {
            
            List<EquipmentEntity> result = new ArrayList<EquipmentEntity>();
            ResultSet resultSet = statement.executeQuery("select section, equipment_id, eq_name,"
            		+ " eq_rcptDate, eq_exprDate, eq_source, eq_complexity from placemap "
            		+ "where eq_name not null;");
            
            while (resultSet.next()) {
                result.add(new EquipmentEntity(resultSet.getInt("equipment_id"),
                                            resultSet.getString("eq_name"),
                                            format.parse(resultSet.getString("eq_rcptDate")),
                                            format.parse(resultSet.getString("eq_exprDate")),
                                            resultSet.getString("eq_source"),
                                            resultSet.getInt("eq_complexity"),
                                            new PlaceEntity(1, resultSet.getString("section"), null)));
            }
            return result;
 
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
	}
	
	public PlaceEntity getPlace(int placeId) {
		
		try (Statement statement = conn.createStatement()) {
            
            ResultSet resultSet = statement.executeQuery("select place_id, section, equipment_id, eq_name,"
            		+ " eq_rcptDate, eq_exprDate, eq_source, eq_complexity from placemap "
            		+ "where place_id = " + placeId + ";");
            while (resultSet.next()) {
                PlaceEntity result = new PlaceEntity(resultSet.getInt("place_id"), resultSet.getString("section"), null);
                
                if(resultSet.getInt("equipment_id") != 0) {
                	result.setCurrentEquipment(
                		new EquipmentEntity(resultSet.getInt("equipment_id"),
                                            resultSet.getString("eq_name"),
                                            format.parse(resultSet.getString("eq_rcptDate")),
                                            format.parse(resultSet.getString("eq_exprDate")),
                                            resultSet.getString("eq_source"),
                                            resultSet.getInt("eq_complexity"),
                                            new PlaceEntity()));
                }
                return result;
            }
            return null;
 
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            return null;
        }
	}
	
public PlaceEntity getPlaceWithEquipment(String nameId) {
		
		try (Statement statement = conn.createStatement()) {
            
            ResultSet resultSet = statement.executeQuery("select place_id, section, equipment_id, eq_name,"
            		+ " eq_rcptDate, eq_exprDate, eq_source, eq_complexity from placemap "
            		+ "where eq_name = \"" + nameId + "\";");
            while (resultSet.next()) {
                PlaceEntity result = new PlaceEntity(resultSet.getInt("place_id"), resultSet.getString("section"), null);
                
                	result.setCurrentEquipment(
                		new EquipmentEntity(resultSet.getInt("equipment_id"),
                                            resultSet.getString("eq_name"),
                                            format.parse(resultSet.getString("eq_rcptDate")),
                                            format.parse(resultSet.getString("eq_exprDate")),
                                            resultSet.getString("eq_source"),
                                            resultSet.getInt("eq_complexity"),
                                            new PlaceEntity()));
                return result;
            }
            return null;
 
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            return null;
        }
	}
	
	public void updatePlace(int placeId, PlaceEntity data){
		
		try(PreparedStatement statement = conn.prepareStatement(
				"update placemap "
				+ "set equipment_id = ?, eq_name = ?,"
				+ "eq_rcptDate = ?, eq_exprDate = ?,"
				+ "eq_source = ?, eq_complexity = ? "
				+ "where place_id = ?;")){
			
			EquipmentEntity eqp = data.getCurrentEquipment();
			statement.setObject(1, eqp != null ? eqp.getName().hashCode() : null);
			statement.setObject(2, eqp != null ? eqp.getName() : null);
			statement.setObject(3, eqp != null ? format.format(eqp.getReceiptDate()) : null);
			statement.setObject(4, eqp != null ? format.format(eqp.getExpiryDate()) : null);
			statement.setObject(5, eqp != null ? eqp.getSource() : null);
			statement.setObject(6, eqp != null ? eqp.getComplexity() : null);
			statement.setObject(7, placeId);
			
			statement.execute();
		}catch (SQLException e) {
            e.printStackTrace();
        }
		
		if(data.getCurrentEquipment() != null)
			try(Statement st = conn.createStatement()){
				
				ResultSet rs;
				List<AcceptanceEntity> accList = data.getCurrentEquipment().getAcceptanceList();
				
				if(accList.size() != 0) {
				AcceptanceEntity accpt = accList.get(accList.size() - 1);
				
				rs = st.executeQuery("select count(*) as total from acceptance where eq_name = \"" + data.getCurrentEquipment().getName() +"\";");
				
				if(rs.getInt("total") == 0) {
					
					PreparedStatement updSt = conn.prepareStatement(
							"insert into acceptance values(?, ?, ?, ?, ?, ?);");
						
						updSt.setObject(1, data.getCurrentEquipment().getName());
						updSt.setObject(2, accpt.getCustomer().getName());
						updSt.setObject(3, accpt.getTechnicalEmployee().getId());
						updSt.setObject(4, accpt.getAccepterEmployee().getId());
						updSt.setObject(5, format.format(accpt.getDateOfAccept()));
						updSt.setObject(6, accpt.getType());
						
						updSt.execute();
					
				}
				}
				
				List<CheckEntity> checks = data.getCurrentEquipment().getTechnicalChecks();
				
				if(checks.size() == 0)
					return;
				
				CheckEntity lastcheck = checks.get(checks.size() - 1);
				
				rs = st.executeQuery("select count(*) as total from checks where ch_id = " + lastcheck.hashCode() + ";");
				if(rs.getInt("total") == 0) { //
					
					PreparedStatement updSt = conn.prepareStatement(
							"insert into checks values( ?, ?, ?, ?, ?, ?);");
						
						updSt.setObject(1, lastcheck.hashCode());
						updSt.setObject(2, data.getCurrentEquipment().getName());
						updSt.setObject(3, lastcheck.getTechnicalEmployee().getId());
						updSt.setObject(4, lastcheck.getResult());
						updSt.setObject(5, lastcheck.isSuccess());
						updSt.setObject(6, format.format(lastcheck.getDate()));
						
						updSt.execute();
					
						
					}
					
				}
		    catch (SQLException e) {
		            e.printStackTrace();
		        }
		
	}
	
	public Map<Integer, PlaceEntity> getAllPlaces(){
		
		try (Statement statement = conn.createStatement()) {
            
			Map<Integer, PlaceEntity> result = new HashMap<>();
            ResultSet resultSet = statement.executeQuery("select place_id, section, equipment_id, eq_name,"
            		+ " eq_rcptDate, eq_exprDate, eq_source, eq_complexity from placemap;");
            while (resultSet.next()) {
            	
            	PlaceEntity newPlace = new PlaceEntity(resultSet.getInt("place_id"), resultSet.getString("section"), null);
                
                if(resultSet.getString("eq_name") != null) {
                	newPlace.setCurrentEquipment(
                		new EquipmentEntity(resultSet.getInt("equipment_id"),
                                            resultSet.getString("eq_name"),
                                            format.parse(resultSet.getString("eq_rcptDate")),
                                            format.parse(resultSet.getString("eq_exprDate")),
                                            resultSet.getString("eq_source"),
                                            resultSet.getInt("eq_complexity"),
                                            new PlaceEntity()));
                }
                result.put(resultSet.getInt("place_id"), newPlace);
            }
            return result;
 
        } catch (SQLException | ParseException e) {
            e.printStackTrace();
            return Collections.emptyMap();
        }
	}
	
	
	public List<PlaceEntity> getEmptyPlaces(){
		
		try (Statement statement = conn.createStatement()) {
            
			List<PlaceEntity> result = new ArrayList<>();
            ResultSet resultSet = statement.executeQuery("select place_id, section, equipment_id, eq_name,"
            		+ " eq_rcptDate, eq_exprDate, eq_source, eq_complexity from placemap where eq_name is null;");
            while (resultSet.next()) {
            	
            	PlaceEntity newPlace = new PlaceEntity(resultSet.getInt("place_id"), resultSet.getString("section"), null);
                
                result.add(newPlace);
            }
            return result;
 
        } catch (SQLException e) {
            e.printStackTrace();
            return Collections.emptyList();
        }
	}

}
