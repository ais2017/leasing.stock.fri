package web.login;

import java.util.HashMap;
import java.util.Map;

public class LoginHandler {

	private Map<String, String> loginDB;
	
	public LoginHandler() {
		
		loginDB = new HashMap<String, String>();
		loginDB.put("responsible", "pass1");
		loginDB.put("ordinary", "pass2");
		loginDB.put("tech", "pass3");
	}
	
	public boolean check(String in_usr, String in_pass) {
		return loginDB.containsKey(in_usr) && loginDB.get(in_usr).equals(in_pass);
	}
}
