package web.login;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import web.login.LoginHandler;

// Implementation of (very) simple authentication mechanism.
// This loops with login page until username-password pair will be correct

@WebServlet("/login")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	LoginHandler hndlr = new LoginHandler();
       
    
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String in_usr = request.getParameter("username");
		String in_pass = request.getParameter("password");
		
		if(hndlr.check(in_usr, in_pass)) {
			switch(in_usr) {
			case "responsible":
				response.sendRedirect("/resp.jsp");
				break;
				
			case "ordinary":
				response.sendRedirect("ord.jsp");
				break;
				
			case "tech":
				response.sendRedirect("tech.jsp");
				break;
			}
		}
		else
			response.sendRedirect("login.html");
	}

}
