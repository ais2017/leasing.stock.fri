package web.gateway;

import com.example.demo.domain.*;
import web.gateway.EquipmentGuiDto;
import java.lang.*;

public class WebTransform {

	public static EquipmentGuiDto transformToDto(Equipment src) {
		String complexity = (new Integer(src.getComplexity())).toString();
		return new EquipmentGuiDto(
				src.getName(), 
				src.getExpiryDate(), 
				src.getReceiptDate(), 
				src.getSource(), 
				complexity, 
				src.getPlace().getSection());
	}
}
