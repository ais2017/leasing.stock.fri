package web.gateway;

import java.util.Date;

import org.springframework.stereotype.Service;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class EquipmentGuiDto {

	public String name;
	public Date exp;
	public Date date;
	public String src;
	public String compl;
	public String place;
}
