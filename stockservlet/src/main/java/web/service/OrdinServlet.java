package web.service;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.domain.Equipment;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.exchange.DataBaseGateway;
import com.example.demo.service.ServiceStorage;

import web.gateway.EquipmentGuiDto;
import web.gateway.WebTransform;

@WebServlet("/OrdinServlet")
public class OrdinServlet extends ServletProcess {
	private static final long serialVersionUID = 1L;

    public OrdinServlet() throws SQLException {
    	
    	super();
		
		mainPageURL = "ord.jsp";
		
    }
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		if(!isAuthorized) {
			response.sendRedirect("login.html");
			isAuthorized = true;
		}
		
		else {
			
			String operation = request.getParameter("op");
			
			switch(operation) {
			
			case "tr":
				tr_operation(request, response);
				break;
				
			case "show":
				show_operation(request, response);
				break;
			}
		}

	}
	
	private void tr_operation(HttpServletRequest request, HttpServletResponse response) {
		
		String from = request.getParameter("fromId");
		String to = request.getParameter("toId");
		
		try{
			storage.transferEquipment(from, to);
			request.setAttribute("message", "Successfully transfered to place " + to);
		}
		catch(RuntimeException e) {
			request.setAttribute("message", e.getMessage()); 
		}
		
		
		request.setAttribute("back", "/ordinary");
		
		try {
			request.getRequestDispatcher("message.jsp").forward(request, response);
		} catch (ServletException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
