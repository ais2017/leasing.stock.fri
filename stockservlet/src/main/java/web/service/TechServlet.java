package web.service;

import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.dto.CheckDto;
import com.example.demo.dto.EquipmentDto;

public class TechServlet extends ServletProcess{

	public TechServlet() throws SQLException {
		super();
		
		mainPageURL = "tech.jsp";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		if(!isAuthorized) {
			response.sendRedirect("login.html");
			isAuthorized = true;
		}
		
		else {
			
			String operation = request.getParameter("op");
			
			switch(operation) {
			
			case "add_chk":
				add_operation(request, response);
				break;
				
			case "show":
				show_operation(request, response);
				break;
			}
		}

	}
	
	private void add_operation(HttpServletRequest request, HttpServletResponse response) {
		
		String employee_id = request.getParameter("id");
		String result = request.getParameter("result");
		boolean success = request.getParameter("success").equals("ok");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date date = null;
		try {
			date = format.parse(request.getParameter("date"));
		} catch (ParseException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}
		
		String equipment_name = request.getParameter("eq_name");
		
		try{
			storage.addCheckOfEquipment(storage.getEquipmentFromStorage(equipment_name), new CheckDto(Integer.parseInt(employee_id), Integer.parseInt(employee_id), result, success, date));
			request.setAttribute("message", "Successfully added");
		}
		catch(RuntimeException e) {
	        request.setAttribute("message", e.getMessage());  
		}
		
		request.setAttribute("back", "/tech");
		
		try {
			request.getRequestDispatcher("message.jsp").forward(request, response);
		} catch (ServletException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
