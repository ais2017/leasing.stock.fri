package web.service;

import java.io.IOException;
import java.net.URL;
import java.sql.SQLException;
import java.util.List;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.domain.Equipment;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.exchange.DataBaseGateway;
import com.example.demo.service.*;

import web.gateway.*;


@WebServlet("/ServletProcess")
public abstract class ServletProcess extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	ServiceStorage storage;
	DataBaseGateway database;
	
	boolean isAuthorized;
	
	String mainPageURL, servletURL;

    public ServletProcess() throws SQLException {
    	
    	database = DataBaseGateway.getInstance();
		database.createInitial();
		
		isAuthorized = false;
	
		storage = new ServiceStorage(database);
		
    }

    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		if(!isAuthorized) {
			response.sendRedirect("login.html");
			isAuthorized = true;
		}
		else
			response.sendRedirect(mainPageURL);

	}
	
	abstract protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException;
	
	
	
	protected void show_operation(HttpServletRequest request, HttpServletResponse response) {
		String filterType = request.getParameter("constraint");
		Predicate<Equipment> filter = e -> !e.getName().isEmpty();
		
		switch(filterType) {
		case "all":
			filter = e -> !e.getName().isEmpty();
			break;
			
		case "name":
			filter = e -> e.getName().equals(request.getParameter("filterParam"));
			break;
			
		case "place":
			filter = e -> e.getPlace().getSection().equals(request.getParameter("filterParam"));
			break;
		}
		
		List<EquipmentGuiDto> lst = storage.getEquipmentForCheck(filter).stream().map(e -> WebTransform.transformToDto(e)).collect(Collectors.toList());
		request.setAttribute("equipments", lst);
		
		try {
			request.getRequestDispatcher(mainPageURL).forward(request, response);
		} catch (ServletException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
