package web.service;

import java.sql.SQLException;

import org.apache.tomcat.util.scan.StandardJarScanner;
import org.eclipse.jetty.apache.jsp.JettyJasperInitializer;
import org.eclipse.jetty.server.Handler;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.server.handler.HandlerList;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.eclipse.jetty.util.component.AbstractLifeCycle;
import org.eclipse.jetty.webapp.Configuration;
import org.eclipse.jetty.webapp.WebAppContext;

import web.login.LoginServlet;

public class Main {
	
	// Handling jsp configuration to use with embedded jetty
	public static class JspStarter extends AbstractLifeCycle implements ServletContextHandler.ServletContainerInitializerCaller
    {
        JettyJasperInitializer sci;
        ServletContextHandler context;
        
        public JspStarter (ServletContextHandler context)
        {
            this.sci = new JettyJasperInitializer();
            this.context = context;
            this.context.setAttribute("org.apache.tomcat.JarScanner", new StandardJarScanner());
        }

        @Override
        protected void doStart() throws Exception
        {
            ClassLoader old = Thread.currentThread().getContextClassLoader();
            Thread.currentThread().setContextClassLoader(context.getClassLoader());
            try
            {
                sci.onStartup(null, context.getServletContext());   
                super.doStart();
            }
            finally
            {
                Thread.currentThread().setContextClassLoader(old);
            }
        }
}

	public static void main(String[] args) throws SQLException {

		int port = 8080;

		Server server = new Server(port);		


		WebAppContext context = new WebAppContext();
		
		// jsp-s are here
		context.setResourceBase("./src/main/webapp");
		
		// usage: localhost:8080<servlet url>
		context.setContextPath("/");
        
		// Responsible employee access servlet
		context.addServlet(new ServletHolder( new RespServlet( ) ),"/main");
		
		// Authentication servlet
		context.addServlet(new ServletHolder( new LoginServlet( ) ),"/login");
		
		// Ordinary employee access servlet
		context.addServlet(new ServletHolder( new OrdinServlet( ) ),"/ordinary");
		
		// Technical employee access servlet
		context.addServlet(new ServletHolder( new TechServlet( ) ),"/tech");
		
		// Add annotation scanning
        Configuration.ClassList classlist = Configuration.ClassList
                .setServerDefault( server );
        classlist.addBefore(
                "org.eclipse.jetty.webapp.JettyWebXmlConfiguration",
                "org.eclipse.jetty.annotations.AnnotationConfiguration" );
        
        context.setAttribute(
                "org.eclipse.jetty.server.webapp.ContainerIncludeJarPattern",
                ".*/[^/]*servlet-api-[^/]*\\.jar$|.*/javax.servlet.jsp.jstl-.*\\.jar$|.*/[^/]*taglibs.*\\.jar$" );

		HandlerList handlers = new HandlerList();
		handlers.setHandlers(new Handler[] { context});
		server.setHandler(handlers);   		

		try {
			server.start();
			System.out.println("Listening port : " + port );
	        
			server.join();
		} catch (Exception e) {
			System.out.println("Error.");
			e.printStackTrace();
		}

	}
	

}
