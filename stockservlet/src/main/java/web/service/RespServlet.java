package web.service;

import java.io.IOException;
import java.util.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.example.demo.dto.EquipmentDto;

public class RespServlet extends ServletProcess{

	public RespServlet() throws SQLException {
		super();
		
		mainPageURL = "resp.jsp";
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		

		if(!isAuthorized) {
			response.sendRedirect("login.html");
			isAuthorized = true;
		}
		
		else {
			
			String operation = request.getParameter("op");
			
			switch(operation) {
			
			case "add":
				add_operation(request, response);
				break;
				
			case "rm":
				rm_operation(request, response);
				break;
				
			case "show":
				show_operation(request, response);
				break;
			}
		}

	}
	
	private void add_operation(HttpServletRequest request, HttpServletResponse response) {
		
		String complexity = request.getParameter("compl");
		String name = request.getParameter("name");
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		Date rcptDate = null, expiry = null;
		
		String techId = request.getParameter("techId");
		String accId = request.getParameter("accId");
		String customer = request.getParameter("customer");
		
		try {
			rcptDate = format.parse(request.getParameter("dateField"));
			expiry = format.parse(request.getParameter("expField"));
		} catch (ParseException e3) {
			// TODO Auto-generated catch block
			e3.printStackTrace();
		}
		
		String source = request.getParameter("source");
		
		
		EquipmentDto equipment1 = new EquipmentDto(5, Integer.parseInt(techId), Integer.parseInt(accId), customer, name, rcptDate, 
				expiry, source, Integer.parseInt(complexity), null);
		
		try{
			String place = storage.addNewEquipment(equipment1).getSection();
			request.setAttribute("message", "Successfully added to place " + place);
		}
		catch(RuntimeException e) {
	        request.setAttribute("message", e.getMessage()); // This will be available as ${message}   
		}
		
		request.setAttribute("back", "/main");
		
		try {
			request.getRequestDispatcher("message.jsp").forward(request, response);
		} catch (ServletException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	private void rm_operation(HttpServletRequest request, HttpServletResponse response) {
		
		String name = request.getParameter("name");
		
		try{
			storage.getEquipmentFromStorageAndClear(name);
			request.setAttribute("message", "Successfully removed");
		}
		catch(RuntimeException e) {
	        request.setAttribute("message", e.getMessage()); // This will be available as ${message}   
		}
		
		request.setAttribute("back", "/main");
		
		try {
			request.getRequestDispatcher("message.jsp").forward(request, response);
		} catch (ServletException | IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}

}
