package com.example.demo.service.transform;

import com.example.demo.dao.entity.EquipmentEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.sql.Date;
import java.time.Instant;
import java.util.ArrayList;

public class EquipmentTransformTest {

    private Equipment equipment;

    @Before
    public void setUp() throws Exception {
        Place place = new Place("123", null);

        equipment = new Equipment(
                "Roasd",
                Date.from(Instant.ofEpochSecond(44444444L)),
                Date.from(Instant.ofEpochSecond(88888888L)),
                "ABC",
                new ArrayList<>(),
                new ArrayList<>(),
                4,
                place
                );
    }

    @Test
    public void transformToEntity() throws Exception {
        EquipmentEntity equipmentEntity = EquipmentTransform.transformToEntity(equipment);
        Equipment test = EquipmentTransform.transformFromEntity(equipmentEntity);
        Assert.assertEquals(equipment, test);
    }



}