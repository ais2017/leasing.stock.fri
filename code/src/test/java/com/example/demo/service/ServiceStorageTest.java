package com.example.demo.service;

import com.example.demo.dao.EmployeeRepository;
import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.service.transform.EquipmentTransform;
import com.example.demo.service.transform.PlaceTransform;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class ServiceStorageTest {



    @Autowired
    EmployeeRepository repository;

    ServiceStorage serviceStorage;
    Equipment equipment1;
    Equipment equipment2;
    Equipment equipment3;
    Equipment equipment4;

    @Before
    public void setUp() throws Exception {
        Map<Integer, PlaceEntity> map = new HashMap<>();
        Place p1 = new Place("1", null);
        Place p2 = new Place("2", null);
        Place p3 = new Place("3", null);
        Place p4 = new Place("4", null);
        Place p5 = new Place("5", null);

        equipment1 = new Equipment("1", null, null, null, 1, p1);
        equipment2 = new Equipment("2", null, null, null, 1, p2);
        equipment3 = new Equipment("3", null, null, null, 1, p3);
        equipment4 = new Equipment("4", null, null, null, 1, p4);

        p1.setEquipment(equipment1);
        p2.setEquipment(equipment2);
        p3.setEquipment(equipment3);
        p4.setEquipment(equipment4);

        map.put(1, PlaceTransform.transformToEntity(p1));
        map.put(2, PlaceTransform.transformToEntity(p2));
        map.put(3, PlaceTransform.transformToEntity(p3));
        map.put(4, PlaceTransform.transformToEntity(p4));
        map.put(5, PlaceTransform.transformToEntity(p5));

        serviceStorage = new ServiceStorage(map, null);

    }

    @Test
    public void checkStorage() throws Exception {
        Assert.assertEquals(serviceStorage.checkStorage().size(), 4);
    }

    @Test
    public void transferEquipment() throws Exception {
        Equipment equipmentFromStorage = serviceStorage.getEquipmentFromStorage(1);
        serviceStorage.transferEquipment(1, 5);
        Assert.assertEquals(serviceStorage.getEquipmentFromStorage(5), equipmentFromStorage);
    }

    @Test
    public void transferEquipmentBusyPlaces() {
        Equipment equipmentFromStorage = serviceStorage.getEquipmentFromStorage(1);
        boolean b = serviceStorage.transferEquipment(1, 2);
        Assert.assertEquals(b, false);
    }


    @Test
    public void getEquipmentForCheck() throws Exception {
        List<Equipment> equipmentForCheck = serviceStorage.getEquipmentForCheck(e -> e.getName().equals("1"));
        Assert.assertEquals(equipmentForCheck.get(0), equipment1);

    }



    @Test
    public void addNewEquipment() throws Exception {
        EquipmentDto equipment5 = new EquipmentDto(5, null, null, null, null, 1, null);
        serviceStorage.addNewEquipment(equipment5);
        Assert.assertEquals(serviceStorage.getEquipmentFromStorage(5), EquipmentTransform.transformFromDto(equipment5));
    }

    @Test(expected = RuntimeException.class)
    public void addNewEquipmentBusyPlaces() throws Exception {
        EquipmentDto equipment5 = new EquipmentDto(5, null, null, null, null,  1, null);
        EquipmentDto equipment6 = new EquipmentDto(6, null, null, null, null, 1, null);
        serviceStorage.addNewEquipment(equipment5);
        serviceStorage.addNewEquipment(equipment6);
        Assert.assertEquals(serviceStorage.getEquipmentFromStorage(5), EquipmentTransform.transformFromDto(equipment5));
    }

    @Test
    public void getEquipmentFromStorage() throws Exception {
        Assert.assertEquals(serviceStorage.getEquipmentFromStorage(2), equipment2);
    }

    @Test(expected = RuntimeException.class)
    public void getEquipmentFromStorageEmptyPlace() {
        Equipment equipmentFromStorage = serviceStorage.getEquipmentFromStorage(5);
    }


    @Test
    public void getEquipmentFromStorageAndClearOk() throws Exception {
        Equipment equipmentFromStorage = serviceStorage.getEquipmentFromStorageAndClear(1);
        Assert.assertEquals(equipmentFromStorage, equipment1);
    }

    @Test(expected = RuntimeException.class)
    public void getEquipmentFromStorageAndClearNotFound() throws Exception {
        Equipment equipmentFromStorage = serviceStorage.getEquipmentFromStorageAndClear(5);
    }


}