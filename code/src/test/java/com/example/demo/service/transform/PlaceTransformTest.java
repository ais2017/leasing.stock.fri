package com.example.demo.service.transform;

import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlaceTransformTest {

    Place place;

    @Before
    public void setUp() throws Exception {
        place = new Place("123", null);
    }

    @Test
    public void transformToEntity() throws Exception {
        Equipment equipment = new Equipment("Test", null, null, null, 1, place);
        place.setEquipment(equipment);
        PlaceEntity entity = PlaceTransform.transformToEntity(place);
        Place res = PlaceTransform.transformFromEntity(entity);
        Assert.assertEquals(place, res);

    }

    @Test
    public void transformToEntityWithNull() throws Exception {
        PlaceEntity entity = PlaceTransform.transformToEntity(place);
        Place res = PlaceTransform.transformFromEntity(entity);
        Assert.assertEquals(place, res);
    }

}