package com.example.demo.service;

import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Check;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dto.CheckDto;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.service.transform.CheckTransform;
import com.example.demo.service.transform.PlaceTransform;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.HashMap;
import java.util.Map;

@RunWith(PowerMockRunner.class)
@PrepareForTest({ CheckTransform.class })
public class ServiceStorageMockito {



    ServiceStorage serviceStorage;
    Equipment equipment1;
    Equipment equipment2;
    Equipment equipment3;
    Equipment equipment4;

    @Before
    public void setUp() throws Exception {
        Map<Integer, PlaceEntity> map = new HashMap<>();
        Place p1 = new Place("1", null);

        equipment1 = new Equipment("1", null, null, null, 1, p1);

        p1.setEquipment(equipment1);


        map.put(1, PlaceTransform.transformToEntity(p1));


        serviceStorage = new ServiceStorage(map, null);

    }

    @Test
    public void addCheckOfEquipment() throws Exception {
        PowerMockito.mockStatic(CheckTransform.class);
        CheckDto checkDto = new CheckDto(1, null, false, null);
        Check value = new Check(null, null, false, null);
        PowerMockito.when(CheckTransform.transformFromDto(checkDto)).thenReturn(value);
        EquipmentDto equipment = new EquipmentDto(1, "1", null, null, null, 1, null);
        serviceStorage.addCheckOfEquipment(equipment, checkDto);
        Assert.assertEquals(serviceStorage.getEquipmentFromStorage(1).getTechnicalChecks().size(), 1);
    }

    @Test
    public void addCheckOfEquipmentWrongEquipment() throws Exception {
        PowerMockito.mockStatic(CheckTransform.class);
        CheckDto checkDto = new CheckDto(1, null, false, null);
        Check value = new Check(null, null, false, null);
        PowerMockito.when(CheckTransform.transformFromDto(checkDto)).thenReturn(value);
        Mockito.when(CheckTransform.transformFromDto(checkDto)).thenReturn(new Check(null, "Ok", false, null));
        boolean ok = serviceStorage.addCheckOfEquipment(new EquipmentDto(7, null, null, null, null,  1, null)
                , checkDto);
        Assert.assertEquals(ok, false);
    }


}
