package com.example.demo.service.transform;

import com.example.demo.dao.entity.CustomerEntity;
import com.example.demo.domain.Customer;
import org.junit.Assert;
import org.junit.Test;

public class CustomerTransformTest {

    private Customer customer = new Customer("Alex", "Test", "Manger");


    @Test
    public void transformToEntity() throws Exception {
        CustomerEntity customerEntity = CustomerTransform.transformToEntity(customer);
        Customer customerRes = CustomerTransform.transformFromEntity(customerEntity);
        Assert.assertEquals(customerRes, customer);
    }

    @Test(expected = NullPointerException.class)
    public void transformFromEntityNull() throws Exception {
        CustomerTransform.transformFromEntity(null);
    }

    @Test(expected = NullPointerException.class)
    public void transformToEntityNull() throws Exception {
        Customer customerRes = CustomerTransform.transformFromEntity(null);
    }

}