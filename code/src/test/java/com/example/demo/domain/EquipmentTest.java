package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class EquipmentTest {


    Equipment equipment;


    @Before
    public void setUp() throws Exception {
        equipment = new Equipment(null, null, null, null, 1, null);
        equipment.addCheck(new Check(null, null, false, null));
        equipment.addAcceptance(new Acceptance(null, null, null, null, "test"));
    }

    @Test
    public void getAcceptanceList() throws Exception {
        Assert.assertEquals(equipment.getAcceptanceList().size(), 1);
    }

    @Test
    public void getTechnicalChecks() throws Exception {
        Assert.assertEquals(equipment.getTechnicalChecks().size(), 1);
    }

    @Test
    public void addAcceptance() throws Exception {
        Assert.assertEquals(equipment.getAcceptanceList().size(), 1);
        Acceptance acceptance = new Acceptance(null, null, null, null, "test");
        equipment.addAcceptance(acceptance);
        Assert.assertEquals(equipment.getAcceptanceList().size(), 2);
        Assert.assertEquals(equipment.getAcceptanceList().contains(acceptance), true);
    }

    @Test
    public void addCheck() throws Exception {
        Assert.assertEquals(equipment.getTechnicalChecks().size(), 1);
        Check check = new Check(null, null, false, null);
        equipment.addCheck(check);
        Assert.assertEquals(equipment.getTechnicalChecks().size(), 2);
        Assert.assertEquals(equipment.getTechnicalChecks().contains(check), true);
    }

}