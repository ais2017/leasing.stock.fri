package com.example.demo.domain;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class PlaceTest {

    Place place;
    Equipment equipment;

    @Before
    public void setUp() throws Exception {
        equipment = new Equipment("1", null, null, null, 1, null);
        place = new Place("1", null);

    }

    @Test
    public void isEmpty() throws Exception {
        Assert.assertEquals(place.isEmpty(), true);
    }

    @Test
    public void setEquipment() throws Exception {
        Assert.assertEquals(place.isEmpty(), true);
        place.setEquipment(equipment);
        Assert.assertEquals(place.getCurrentEquipment(), equipment);
    }

    @Test
    public void getAndClear() throws Exception {
        place.setEquipment(equipment);
        Equipment andClear = place.getAndClear();
        Assert.assertEquals(andClear, equipment);
        Assert.assertEquals(place.isEmpty(), true);
    }

}
