package com.example.demo.entity;

import com.example.demo.domain.Acceptance;
import com.example.demo.domain.Customer;
import com.example.demo.domain.Employee;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.time.Instant;
import java.util.Date;

public class AcceptanceTest {

    Acceptance acceptance;
    Customer customer;
    Employee employee1;
    Employee employee2;
    Date date;

    @Before
    public void setUp() throws Exception {
        customer = new Customer("Ivan", "Ivanov", "manager");
        employee1 = new Employee("employee1", "test1", "tes1", "technicalEmployee", "111");
        employee2 = new Employee("employee2", "test2", "tes2", "accepterEmployee", "222");
        date = Date.from(Instant.now());
        acceptance = new Acceptance(customer, employee1, employee2, date , "Accept");

    }

    @Test
    public void getCustomer() throws Exception {
        Assert.assertEquals(acceptance.getCustomer(), customer);
    }

    @Test
    public void getTechnicalEmployee() throws Exception {
        Assert.assertEquals(acceptance.getTechnicalEmployee(), employee1);

    }

    @Test
    public void getAccepterEmployee() throws Exception {
        Assert.assertEquals(acceptance.getAccepterEmployee(), employee2);

    }

    @Test
    public void getDateOfAccept() throws Exception {
        Assert.assertEquals(acceptance.getDateOfAccept(), date);

    }

    @Test
    public void getType() throws Exception {
        Assert.assertEquals(acceptance.getType(), "Accept");
    }

}