package com.example.demo.service.transform;

import com.example.demo.dao.entity.EquipmentEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.dto.EquipmentDto;

import java.util.stream.Collectors;

public abstract class EquipmentTransform {


    public static EquipmentEntity transformToEntity(Equipment equipment) {
        return new EquipmentEntity(
                null,
                equipment.getName(),
                equipment.getReceiptDate(),
                equipment.getExpiryDate(),
                equipment.getSource(),
                equipment.getAcceptanceList().stream().map(AcceptanceTransform::transformToEntity).collect(Collectors.toList()),
                equipment.getTechnicalChecks().stream().map(CheckTransform::transformToEntity).collect(Collectors.toList()),
                equipment.getComplexity(),
                null
                );
    }

    public static Equipment transformFromEntity(EquipmentEntity equipmentEntity) {
        return new Equipment(
                equipmentEntity.getName(),
                equipmentEntity.getReceiptDate(),
                equipmentEntity.getExpiryDate(),
                equipmentEntity.getSource(),
                equipmentEntity.getAcceptanceList().stream().map(AcceptanceTransform::transformFromEntity).collect(Collectors.toList()),
                equipmentEntity.getTechnicalChecks().stream().map(CheckTransform::transformFromEntity).collect(Collectors.toList()),
                equipmentEntity.getComplexity(),
                null
                );
    }



    public static Equipment transformFromDto(EquipmentDto equipment) {
        return new Equipment(
                equipment.getName(),
                equipment.getReceiptDate(),
                equipment.getExpiryDate(),
                equipment.getSource(),
                equipment.getComplexity(),
                null
        );
    }
}
