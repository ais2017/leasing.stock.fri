package com.example.demo.service.transform;

import com.example.demo.dao.entity.EmployeeEntity;
import com.example.demo.domain.Employee;

public abstract class EmployeeTransform {

    public static EmployeeEntity transformToEntity(Employee employee) {
        return new EmployeeEntity(null,
                employee.getName(),
                employee.getSurname(),
                employee.getSecondName(),
                employee.getPosition(),
                employee.getAddress());
    }

    public static Employee transformFromEntity(EmployeeEntity employeeEntity) {
        return new Employee(employeeEntity.getName(),
                employeeEntity.getSurname(),
                employeeEntity.getSecondName(),
                employeeEntity.getPosition(),
                employeeEntity.getAddress());
    }

}
