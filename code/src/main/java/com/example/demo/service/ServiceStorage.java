package com.example.demo.service;

import com.example.demo.dao.entity.EmployeeEntity;
import com.example.demo.dao.entity.PlaceEntity;
import com.example.demo.domain.Equipment;
import com.example.demo.domain.Place;
import com.example.demo.dto.CheckDto;
import com.example.demo.dto.EquipmentDto;
import com.example.demo.service.transform.CheckTransform;
import com.example.demo.service.transform.EquipmentTransform;
import com.example.demo.service.transform.PlaceTransform;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class ServiceStorage {



    private Map<Integer, PlaceEntity> placeMap;
    private Map<Integer, EmployeeEntity> employeeMap;


    public List<Equipment> checkStorage() {
        return placeMap.values().stream()
                .map(PlaceEntity::getCurrentEquipment)
                .filter(s -> s != null)
                .map(EquipmentTransform::transformFromEntity)
                .collect(Collectors.toList());
    }

    public boolean transferEquipment(Integer fromPlaceId, Integer toPlaceId) {
        Place fromPlace = PlaceTransform.transformFromEntity(placeMap.get(fromPlaceId));
        Place toPlace = PlaceTransform.transformFromEntity(placeMap.get(toPlaceId));

        if (!fromPlace.isEmpty() && toPlace.isEmpty()) {
            toPlace.setEquipment(fromPlace.getAndClear());
            placeMap.put(fromPlaceId, PlaceTransform.transformToEntity(fromPlace));
            placeMap.put(toPlaceId, PlaceTransform.transformToEntity(toPlace));
            return true;
        }
        return false;

    }

    public List<Equipment> getEquipmentForCheck(Predicate<Equipment> filter) {
        List<Equipment> equipments = Collections.unmodifiableList(placeMap.values().stream()
                .map(PlaceEntity::getCurrentEquipment)
                .filter(e -> e != null)
                .map(EquipmentTransform::transformFromEntity).collect(Collectors.toList()));
        return equipments.stream().filter(filter).collect(Collectors.toList());
    }

    public boolean addCheckOfEquipment(EquipmentDto equipmentDto, CheckDto check) {
        Equipment equipment = EquipmentTransform.transformFromDto(equipmentDto);
        Map<Integer, Place> collect = placeMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> PlaceTransform.transformFromEntity(e.getValue())));
        for (Map.Entry<Integer, Place> current : collect.entrySet()) {
            Place value = current.getValue();
            if(value.getCurrentEquipment() != null && value.getCurrentEquipment().equals(equipment)) {
                value.getCurrentEquipment().addCheck(CheckTransform.transformFromDto(check));
                PlaceEntity entity = PlaceTransform.transformToEntity(value);
                placeMap.put(current.getKey(), entity);
                return true;
            }
        }
        return false;
    }

    public Place addNewEquipment(EquipmentDto equipmentDto) {
        Equipment equipment = EquipmentTransform.transformFromDto(equipmentDto);
        Map<Integer, Place> collect = placeMap.entrySet().stream()
                .collect(Collectors.toMap(Map.Entry::getKey, e -> PlaceTransform.transformFromEntity(e.getValue())));
        for (Map.Entry<Integer, Place> placeEntry : collect.entrySet()) {
            Place value = placeEntry.getValue();
            if (value.isEmpty()) {
                value.setEquipment(equipment);
                placeMap.put(placeEntry.getKey(), PlaceTransform.transformToEntity(value));
                return value;
            }
        }
        throw new RuntimeException("Not free places");
    }

    public Equipment getEquipmentFromStorage(Integer placeId) {
        Place place = PlaceTransform.transformFromEntity(placeMap.get(placeId));
        if (!place.isEmpty()) {
            return place.getCurrentEquipment();
        } else {
            throw new RuntimeException("Empty place");
        }
    }

    public Equipment getEquipmentFromStorageAndClear(Integer placeId) {
        Place place = PlaceTransform.transformFromEntity(placeMap.get(placeId));
        if (!place.isEmpty()) {
            Equipment equip = place.getAndClear();
            placeMap.put(placeId, PlaceTransform.transformToEntity(place));
            return equip;
        } else {
            throw new RuntimeException("Empty place");
        }
    }


}
