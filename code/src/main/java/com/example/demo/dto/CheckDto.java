package com.example.demo.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@AllArgsConstructor
@Getter
public class CheckDto {

    private final Integer id;
    private final String result;
    private final boolean isSuccess;
    private final Date date;


}
