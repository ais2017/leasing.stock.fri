package com.example.demo.dao;

import com.example.demo.dao.entity.EquipmentEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EquipmentRepository extends JpaRepository<EquipmentEntity, Integer> {

    EquipmentEntity findById(Integer id);

}
