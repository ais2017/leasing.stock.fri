package com.example.demo.dao;

import com.example.demo.dao.entity.CheckEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CheckRepository extends JpaRepository<CheckEntity, Integer> {
}
