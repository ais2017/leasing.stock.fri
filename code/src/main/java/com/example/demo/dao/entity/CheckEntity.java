package com.example.demo.dao.entity;

import groovy.transform.ToString;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import java.util.Date;

@Entity
@ToString
@AllArgsConstructor
@Getter
@Setter
public class CheckEntity {

    @Id
    @GeneratedValue
    private Integer id;
    @OneToOne
    private EmployeeEntity technicalEmployee;
    private String result;
    private boolean isSuccess;
    private Date date;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CheckEntity that = (CheckEntity) o;

        if (isSuccess != that.isSuccess) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (technicalEmployee != null ? !technicalEmployee.equals(that.technicalEmployee) : that.technicalEmployee != null)
            return false;
        if (result != null ? !result.equals(that.result) : that.result != null) return false;
        return date != null ? date.equals(that.date) : that.date == null;

    }

    @Override
    public int hashCode() {
        int result1 = id != null ? id.hashCode() : 0;
        result1 = 31 * result1 + (technicalEmployee != null ? technicalEmployee.hashCode() : 0);
        result1 = 31 * result1 + (result != null ? result.hashCode() : 0);
        result1 = 31 * result1 + (isSuccess ? 1 : 0);
        result1 = 31 * result1 + (date != null ? date.hashCode() : 0);
        return result1;
    }
}
