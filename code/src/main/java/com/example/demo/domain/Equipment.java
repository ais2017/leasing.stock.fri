package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

@AllArgsConstructor
public class Equipment {


    @Getter
    private final String name;
    @Getter
    private final Date receiptDate;
    @Getter
    private final Date expiryDate;
    @Getter
    private final String source;
    private final List<Acceptance> acceptanceList;
    private final List<Check> technicalChecks;
    @Getter
    private final int complexity;
    @Getter
    private Place place;

    public Equipment(String name, Date receiptDate, Date expiryDate, String source, int complexity, Place place) {
        this.name = name;
        this.receiptDate = receiptDate;
        this.expiryDate = expiryDate;
        this.source = source;
        this.complexity = complexity;
        this.place = place;
        acceptanceList = new ArrayList<>();
        technicalChecks = new ArrayList<>();
    }

    public List<Acceptance> getAcceptanceList() {
        return Collections.unmodifiableList(acceptanceList);
    }

    public List<Check> getTechnicalChecks() {
        return Collections.unmodifiableList(technicalChecks);
    }

    public boolean addAcceptance(Acceptance acceptance) {
        return acceptanceList.add(acceptance);
    }

    public boolean addCheck(Check check) {
        return technicalChecks.add(check);
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Equipment equipment = (Equipment) o;

        if (complexity != equipment.complexity) return false;
        if (name != null ? !name.equals(equipment.name) : equipment.name != null) return false;
        if (receiptDate != null ? !receiptDate.equals(equipment.receiptDate) : equipment.receiptDate != null)
            return false;
        if (expiryDate != null ? !expiryDate.equals(equipment.expiryDate) : equipment.expiryDate != null) return false;
        if (source != null ? !source.equals(equipment.source) : equipment.source != null) return false;
        if (acceptanceList != null ? !acceptanceList.equals(equipment.acceptanceList) : equipment.acceptanceList != null)
            return false;
        return technicalChecks != null ? technicalChecks.equals(equipment.technicalChecks) : equipment.technicalChecks == null;

    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (receiptDate != null ? receiptDate.hashCode() : 0);
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        result = 31 * result + (source != null ? source.hashCode() : 0);
        result = 31 * result + (acceptanceList != null ? acceptanceList.hashCode() : 0);
        result = 31 * result + (technicalChecks != null ? technicalChecks.hashCode() : 0);
        result = 31 * result + complexity;
        return result;
    }
}
