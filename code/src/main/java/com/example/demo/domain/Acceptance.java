package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;

@Getter
@AllArgsConstructor
public class Acceptance {

    private final Customer customer;
    private final Employee technicalEmployee;
    private final Employee accepterEmployee;
    private final Date dateOfAccept;
    private final String type;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Acceptance that = (Acceptance) o;

        if (customer != null ? !customer.equals(that.customer) : that.customer != null) return false;
        if (technicalEmployee != null ? !technicalEmployee.equals(that.technicalEmployee) : that.technicalEmployee != null)
            return false;
        if (accepterEmployee != null ? !accepterEmployee.equals(that.accepterEmployee) : that.accepterEmployee != null)
            return false;
        if (dateOfAccept != null ? !dateOfAccept.equals(that.dateOfAccept) : that.dateOfAccept != null) return false;
        return type != null ? type.equals(that.type) : that.type == null;

    }

    @Override
    public int hashCode() {
        int result = customer != null ? customer.hashCode() : 0;
        result = 31 * result + (technicalEmployee != null ? technicalEmployee.hashCode() : 0);
        result = 31 * result + (accepterEmployee != null ? accepterEmployee.hashCode() : 0);
        result = 31 * result + (dateOfAccept != null ? dateOfAccept.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        return result;
    }
}
