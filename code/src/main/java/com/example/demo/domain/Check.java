package com.example.demo.domain;

import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Date;


@AllArgsConstructor
@Getter
public class Check {


    private final Employee technicalEmployee;
    private final String result;
    private final boolean isSuccess;
    private final Date date;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Check check = (Check) o;

        if (isSuccess != check.isSuccess) return false;
        if (result != null ? !result.equals(check.result) : check.result != null) return false;
        return date != null ? date.equals(check.date) : check.date == null;

    }

    @Override
    public int hashCode() {
        int result1 = result != null ? result.hashCode() : 0;
        result1 = 31 * result1 + (isSuccess ? 1 : 0);
        result1 = 31 * result1 + (date != null ? date.hashCode() : 0);
        return result1;
    }
}
